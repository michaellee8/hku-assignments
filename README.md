# HKU Assignments

These are my assignments in HKU CS courses, with my test scripts and corresponding test cases.

All source code of my solution (those in `src`) are in GPL v3.0 license. Specifically, if you took my code as your
assignment, you must declare that it came from here, or you may face legal consequences. However, if you just read my
code and write your own, I will be very glad that you took my help.

All source code of the test scripts (those in `scripts`) are in MIT license. It means that you can use them without
much restrictions.

Other files maybe from HKU. I may not hold copyrights to them. Please don't use them if you are not allowed to do so.