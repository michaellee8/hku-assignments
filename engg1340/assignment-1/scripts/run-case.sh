#!/usr/bin/env bash

# This scripts runs a specific test case. E.g. ./run-case.sh 1 1

Q=$1
C=$2
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [[ -e ${DIR}/../tmp/${Q}_${C}.run.out.txt ]];then
  rm ${DIR}/../tmp/${Q}_${C}.run.out.txt
fi
${DIR}/../out/${Q}.run < ${DIR}/../test-case/${Q}_${C}.in.txt > ${DIR}/../tmp/${Q}_${C}.run.out.txt
DIFF_OUTPUT=$(diff --brief ${DIR}/../tmp/${Q}_${C}.run.out.txt ${DIR}/../test-case/${Q}_${C}.out.txt)
if [[ -z "$DIFF_OUTPUT" ]];then
  echo "[PASS] Test case $Q-$C passed"
else
  echo "[FAIL] Test case $Q-$C failed, diff is shown below"
  diff ${DIR}/../tmp/${Q}_${C}.run.out.txt ${DIR}/../test-case/${Q}_${C}.out.txt
fi
