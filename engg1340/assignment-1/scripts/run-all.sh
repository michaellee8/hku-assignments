#!/usr/bin/env bash

# This script builds all the solution and run all of the test cases.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

${DIR}/run-q.sh 1 8
${DIR}/run-q.sh 2 8
${DIR}/run-q.sh 3 8
${DIR}/run-q.sh 4 10
${DIR}/run-q.sh 5 10
${DIR}/run-q.sh 6 8
exit 0