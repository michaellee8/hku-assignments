#include <iostream>
#include <cstdio>
using namespace std;
int count_tri(int n);

int main() {
  int m,n;
  cin >> m >> n;
  for (int i=m;i<=n;i++){
    int c = count_tri(i);
    if (c!=0){
      printf("%d %d\n",i,c);
    }
  }
}

int count_tri(int n){
  int count = 0;
  for (int i=2;i<n;i++){
    for (int j=1;j<n-i;j++){
      int k = n - i - j;
      if (k*k==i*i+j*j){
        count++;
        // printf("[debug] %d %d %d %d\n",i,j,k,count);
      }else if(i*i==k*k+j*j){
        count++;
        // printf("[debug] %d %d %d %d\n",i,j,k,count);
      }else if(j*j==i*i+k*k){
        count++;
        // printf("[debug] %d %d %d %d\n",i,j,k,count);
      }
    }
  }
  return count/6;
}
