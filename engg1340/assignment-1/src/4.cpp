#include <iostream>
using namespace std;
class Rect{
  public:
  double x_neg,x_pos,y_neg,y_pos;
  Rect();
  Rect(double x_neg,double x_pos,double y_neg,double y_pos);
  void extend_to_cover(double x, double y);
  private:
  bool inited;
};
int main() {
  Rect r;
  while (true){
    string cmd;
    cin >> cmd;
    if (cmd == "#"){
      break;
    }else if (cmd == "R"){
      double x,y,w,h;
      cin >> x >> y >> w >> h;
      r.extend_to_cover(x-w/2,y-h/2);
      r.extend_to_cover(x+w/2,y-h/2);
      r.extend_to_cover(x-w/2,y+h/2);
      r.extend_to_cover(x+w/2,y+h/2);
      continue;
    }else if (cmd == "C"){
      double x,y,rad;
      cin >> x >> y >> rad;
      r.extend_to_cover(x-rad,y-rad);
      r.extend_to_cover(x+rad,y-rad);
      r.extend_to_cover(x-rad,y+rad);
      r.extend_to_cover(x+rad,y+rad);
      continue;
    }else if (cmd == "P"){
      int n;
      cin >> n;
      for (int i = 0;i<n;i++){
        double x,y;
        cin >> x >> y;
        r.extend_to_cover(x,y);
      }
    }
  }
  printf(
    "%g %g %g %g\n",
    (r.x_neg+r.x_pos)/2,
    (r.y_neg+r.y_pos)/2,
    r.x_pos-r.x_neg,
    r.y_pos-r.y_neg
  );
}


void Rect::extend_to_cover(double x, double y){
  if (!this->inited){
    this->inited = true;
    this->x_neg = x;
    this->x_pos = x;
    this->y_neg = y;
    this->y_pos = y;
    return;
  }
  if (x < this->x_neg){
    this->x_neg = x;
  }
  if (x > this->x_pos){
    this->x_pos = x;
  }
  if (y < this->y_neg){
    this->y_neg = y;
  }
  if (y > this->y_pos){
    this->y_pos = y;
  }
}

Rect::Rect(double x_neg,double x_pos,double y_neg,double y_pos){
  this->inited = true;
  this->x_neg = x_neg;
  this->x_pos = x_pos;
  this->y_neg = y_neg;
  this->y_pos = y_pos;
}

Rect::Rect(){
  this->inited = false;
}
