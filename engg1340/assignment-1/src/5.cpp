#include <iostream>
#include <string>
using namespace std;


bool pal_num_test(int n);
bool pro_test(int n);

int main() {
  int m,n;
  cin >> m >> n;
  for (int i=m;i<=n;i++){
    if (pal_num_test(i) && pro_test(i)){
      cout << i << endl;
    }
  }
}

bool pal_num_test(int n){
  string s = to_string(n);
  return s == string(s.rbegin(),s.rend());
}

bool pro_test(int n){
  int lim = n>999?999:n;
  for (int i=100;i<=lim;i++){
    if (n%i == 0 && n/i>=100 && n/i<=999){
      return true;
    }
  }
  return false;
}
