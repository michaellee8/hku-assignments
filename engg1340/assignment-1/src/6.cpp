#include <iostream>
using namespace std;

int main() {
  char cmd;
  cin >> cmd;
  int pos;
  cin >> pos;
  if (cmd=='d'){
    pos *= -1;
  }
  while (true){
    char c;
    cin >> c;
    if (c=='!'){
        break;
    }
    if ('a' <= c && c <= 'z'){
      cout << (char)((c-'a'+pos+26)%26+'A');
    }else if ('A' <= c && c <= 'Z'){
      cout << (char)((c-'A'+pos+26)%26+'a');
    }else {
      cout << c;
    }
  }
}
