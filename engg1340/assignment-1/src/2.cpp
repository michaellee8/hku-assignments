#include <iostream>
#include <cstdio>
using namespace std;

long long int fact(long long int n);
long long int power(long long int x, long long int n);

int main() {
  long long int x,n;
  cin >> x >> n;
  double sum = 0.0;
  for (long long int i=0;i<=n;i++){
    sum += power(x,i)/(double)fact(i);
    // printf("[debug] %lld %lld %f\n",power(x,i),fact(i),power(x,i)/(double)fact(i));
    printf("%lld %.10f\n",i,sum);
  }

}

long long int fact( long long int n){
  if (n==0){
    return 1;
  }else {
    return n*fact(n-1);
  }
}

long long int power(long long int x,long long int n){
  if (n==0){
    return 1;
  }else{
    return x*power(x,n-1);
  }
}
