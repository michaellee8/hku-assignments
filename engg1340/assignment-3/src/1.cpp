// Copyright by Michael Lee under GPL v3.0, all rights reserved

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

typedef int ml8;

class Player {
public:
    int n;
    Player *next;

    bool isOnly();

    void removeNext();

    static Player *ml8buildList(ml8);
};

bool Player::isOnly() {
    return this == next;
}

void Player::removeNext() {
    auto newNext = this->next->next;
//    cout << "eliminated" << this->next->n << endl;
    delete this->next;
    this->next = newNext;
}

Player *Player::ml8buildList(int n) {
    auto head = new Player();
    head->n = 1;
    auto curr = head;
    for (int i = 2; i <= n; i++) {
        auto newPlayer = new Player();
        newPlayer->n = i;
        curr->next = newPlayer;
        curr = newPlayer;
    }
    curr->next = head;
    return head;
}

void ml8debug(int i){
    cout << i;
}

void ml8debug(){
    // cout << "triggered debug";
}

int main() {
    int n, k;
    // ml8debug();
    cin >> n >> k;
    auto curr = Player::ml8buildList(n);
    while (!curr->isOnly()) {
        for (int i = 0; i < k - 2; i++) {
            curr = curr->next;
            
        }
        curr->removeNext();
       // ml8debug(curr->n);
        curr = curr->next;
      ml8debug();
    } 
   // ml8debug
    cout << curr->n << endl;
    return 0;
}
