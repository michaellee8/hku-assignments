#!/usr/bin/env bash

# This scripts runs a specific test case. E.g. ./run-case.sh 1 1

Q=$1
C=$2
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [[ -e ${DIR}/../tmp/${Q}-${C}/ ]];then
  rm -r ${DIR}/../tmp/${Q}-${C}/
fi

# Clone the workspace
cp -r ${DIR}/../test-case/${Q}-${C}/ ${DIR}/../tmp/${Q}-${C}/
cp ${DIR}/../out/${Q}.run ${DIR}/../tmp/${Q}-${C}/${Q}.run

# Change directory to workspace
cd ${DIR}/../tmp/${Q}-${C}/

OUTPUT_FILES=$(find ./ -maxdepth 1 -name '*.output.txt' | cut -b 3- | cut -d. -f1)

# Test stdout first
./${Q}.run < ./${Q}-${C}.in.txt > ./${Q}-${C}.run.out.txt
DIFF_OUTPUT=$(diff --brief ./${Q}-${C}.run.out.txt ./${Q}-${C}.out.txt)
if [[ -z "$DIFF_OUTPUT" ]];then
  echo "[PASS] Test case $Q-$C stdout passed"
else
  echo "[FAIL] Test case $Q-$C stdout failed, diff is shown below"
  diff ./${Q}-${C}.run.out.txt ./${Q}-${C}.out.txt
fi

for filename in ${OUTPUT_FILES}; do
  DIFF_OUTPUT=$(diff --brief ./${filename}.output.txt ./${filename}.txt)
  if [[ -z "$DIFF_OUTPUT" ]];then
    echo "[PASS] Test case $Q-$C $filename passed"
  else
    echo "[FAIL] Test case $Q-$C $filename failed, diff is shown below"
    diff ./${filename}.output.txt ./${filename}.txt
  fi
done