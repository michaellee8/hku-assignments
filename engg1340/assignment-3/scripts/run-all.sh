#!/usr/bin/env bash

# This script builds all the solution and run all of the test cases.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

${DIR}/run-q.sh 1 8
exit 0