//
//  Assignment 2 Task 3 template
//  Copyright © 2019 HKU ENGG1340. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

const int MAX_NUM_STOPWORDS = 100;

// a structure for storing a stop_words and its corresponding removal count
struct Stop_word
{
  string word;  // stop word
  int count;    // removal count
};

///////// DO NOT MODIFY ANYTHING ABOVE THIS LINE /////////

// IMPORTANT:  Do NOT change any of the function headers
//             It means that you will need to use the function headers as is


// You may implement additional functions here



//
// ReadStopWordFromFile:  read stop words from a file and
//                store them in an array of struct Stop_word
//                (also initialize removal count of each stop word to zero)
// input:   stop_word_filename - name of the file containing stop words
// output:  words[] - an array to store the stop words read from input file
//          num_words - number of stop words read from file
void ReadStopWordFromFile(string stop_word_filename, Stop_word words[], int &num_words)
{
  // ==========================

  // Complete the function here

  // ==========================
}

//
// WriteStopWordCountToFile:  write stop words and removal counts to a file
//
// input: wordcount_filename - name of the file to write to
//        words[] - an array storing the stop words and removal counts
//        num_words - number of stop words in words[]
//
void WriteStopWordCountToFile(string wordcount_filename, Stop_word words[], int num_words)
{
  // ==========================

  // Complete the function here

  // ==========================
}



//
// RemoveWordFromLine: to delete all occurrences of a word from a line of text
//
// input: line - a string of text to be processed
//        word - a word whose occurrences are to be removed from line
// output: an integer for the number of times the word is being removed
//
int RemoveWordFromLine(string &line, string word)
{
  // ==========================

  // Complete the function here

  // ==========================
}

//
// RemoveAllStopwordsFromLine: deleting all occurrences of all stop words from
//      a given line of text
//      this function calls RemoveWordFromLine() to remove all occurrences of
//      a stop word,
//
// input:  line - a string of text to process
//         words[] - an array of stop words to remove from line
//         num_words - number of stop words stored in words[]
// output: line - with all stop words removed
// return: an integer for number of words removed from line
//
int RemoveAllStopwordsFromLine(string &line, Stop_word words[], int num_words)
{
  // ==========================

  // Complete the function here

  // ==========================
}


int main() {

  Stop_word stopwords[MAX_NUM_STOPWORDS];     // an array of struct Stop_word

  // read in two filenames from user input



  // read stop words from stopword file and
  // store them in an array of struct Stop_word



  // open text file



  // open cleaned text file



  // read in each line from text file, remove stop words,
  // and write to output cleaned text file



  // close text file and cleaned text file



  // write removal count of stop words to files



  // output to screen total number of words removed




  return 0;
}
