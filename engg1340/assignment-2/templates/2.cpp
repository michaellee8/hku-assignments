//
//  Assignment 2 Task 2 template
//  Copyright © 2019 HKU ENGG1340. All rights reserved.
//


#include <iostream>

using namespace std;

const int MAX_SIZE = 1000;


///////// DO NOT MODIFY ANYTHING ABOVE THIS LINE /////////

// IMPORTANT:  Do NOT change any of the function headers already provided to you
//             It means that you will need to use the function headers as is


// You may implement additional functions here



// Function: find the smallest number of steps to go from the starting point
//           to the destination in a given map.
//
// Input: int map[][]: 2D-array map
//        int map_h: the height of the map
//        int map_w: the width of the map
// Output: return true if a path is found, and store the smallest number of
//                      steps taken in &num_steps (pass-by-reference)
//         return false if there is no path
// ==============================================================
bool FindPath(int map[][MAX_SIZE], int map_h, int map_w, int &num_steps)
{
  // ==========================

  // Complete the function here

  // ==========================
}


///////// DO NOT MODIFY ANYTHING BELOW THIS LINE /////////

// Function: main function
// ==============================================================
int main()
{
  int map_h;
  int map_w;
  cin >> map_h >> map_w;

  int map[MAX_SIZE][MAX_SIZE];

  // initialize map
  for (int i=0; i<MAX_SIZE; i++)
    for (int j=0; j<MAX_SIZE; j++)
      map[i][j] = -1;

  // read map from standard input
  for (int i=0; i<map_h; i++)
    for (int j=0; j<map_w; j++)
      cin >> map[i][j];

  int steps;
  // print to screen number of steps if a path is found, otherwise print "No"
  if (FindPath(map, map_h, map_w, steps))
    cout << steps << endl;
  else
    cout << "No" << endl;

}
