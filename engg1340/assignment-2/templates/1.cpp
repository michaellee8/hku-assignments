//
//  Assignment 2 Task 1 template
//  Copyright © 2019 HKU ENGG1340. All rights reserved.
//

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

const double e=2.72;
const int MAX_T = 100;

///////// DO NOT MODIFY ANYTHING ABOVE THIS LINE /////////

// IMPORTANT:  Do NOT change any of the function headers
//             It means that you will need to use the function headers as is

// You may implement additional functions here


// Function: sigmoid activation function
// Input: double x: the input of sigmoid activation function
// Ouput: the output of sigmoid activation function
double sigmoid(double x)
{
  // ==========================

  // Complete the function here

  // ==========================
}

// Function: tanh activate function
// Input: double x: the input of tanh activation function
// Ouput: double: the output of tanh activation function.
double tanh(double x)
{
  // ==========================

  // Complete the function here

  // ==========================
}

// Function: compute the next hidden value in an RNN cell
// Input: double x: current input value
//        double h: current hidden status in RNN cell
// Ouput: double: next hidden value in RNN cell
double ComputeH(double x, double h)
{
  // ==========================

  // Complete the function here

  // ==========================
}

// Function: compute the output value at current time step
// Input: double x: current input value
//        double h: current hidden status in RNN cell
// Ouput: double: current output value
double ComputeO(double x, double h)
{
  // ==========================

  // Complete the function here

  // ==========================

}

// Function: print the values stored in a 1D-array to screen
// Input: double xs[]: the value of the sequence
//        int seq_len: the length of the sequence
void PrintSeqs(double xs[], int seq_len)
{
  // ==========================

  // Complete the function here

  // ==========================
}

// Function: main function
int main()
{
  // ==========================

  // Complete the function here

  // ==========================

}
