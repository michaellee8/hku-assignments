//
//  Assignment 2 Task 3 template
//  Copyright © 2019 HKU ENGG1340. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

const int MAX_NUM_STOPWORDS = 100;

// a structure for storing a stop_words and its corresponding removal count
struct Stop_word {
    string word;  // stop word
    int count;    // removal count
};

///////// DO NOT MODIFY ANYTHING ABOVE THIS LINE /////////

// IMPORTANT:  Do NOT change any of the function headers
//             It means that you will need to use the function headers as is


// You may implement additional functions here




//
// ReadStopWordFromFile:  read stop words from a file and
//                store them in an array of struct Stop_word
//                (also initialize removal count of each stop word to zero)
// input:   stop_word_filename - name of the file containing stop words
// output:  words[] - an array to store the stop words read from input file
//          num_words - number of stop words read from file
void ReadStopWordFromFile(string stop_word_filename, Stop_word words[], int &num_words) {
    ifstream stop_word_file(stop_word_filename);
    if (!stop_word_file.is_open()) {
        cout << "Failed to open " << stop_word_filename << endl;
        exit(0);
    }
    string s;
    int i = 0;
    while (getline(stop_word_file, s)) {
        words[i].count = 0;
        words[i].word = s;
        i++;
    }
    num_words = i;
    stop_word_file.close();
}

//
// WriteStopWordCountToFile:  write stop words and removal counts to a file
//
// input: wordcount_filename - name of the file to write to
//        words[] - an array storing the stop words and removal counts
//        num_words - number of stop words in words[]
//
void WriteStopWordCountToFile(string wordcount_filename, Stop_word words[], int num_words) {
    ofstream wordcount_file(wordcount_filename);
    if (!wordcount_file.is_open()) {
        cout << "Failed to open " << wordcount_filename << endl;
        exit(0);
    }
    for (int i = 0; i < num_words; i++) {
        wordcount_file << words[i].word << " " << words[i].count << endl;
    }
    wordcount_file.close();
}


//
// RemoveWordFromLine: to delete all occurrences of a word from a line of text
//
// input: line - a string of text to be processed
//        word - a word whose occurrences are to be removed from line
// output: an integer for the number of times the word is being removed
//
int RemoveWordFromLine(string &line, string word) {
    int count = 0;
    auto words_size = line.length();
    auto words = new string[words_size];
    int word_i = 0;
    string current_word = "";
    for (int i = 0; i < line.length(); i++) {
        if (line[i] == ' ') {
            words[word_i] = current_word;
            current_word = "";
            word_i++;
            continue;
        }
        current_word += line[i];
    }
    words[word_i] = current_word;
    word_i++;
    for (int i = 0; i < word_i; i++) {
        if (words[i] == word) {
            words[i] = "";
            count++;
        }
    }
    line = "";
    for (int i = 0; i < word_i; i++) {
        line += words[i];
        line += " ";
    }

    while (line[line.length() - 1] == ' ') {
        line = line.substr(0, line.length() - 1);
    }
    delete[] words;
    return count;
}

//
// RemoveAllStopwordsFromLine: deleting all occurrences of all stop words from
//      a given line of text
//      this function calls RemoveWordFromLine() to remove all occurrences of
//      a stop word,
//
// input:  line - a string of text to process
//         words[] - an array of stop words to remove from line
//         num_words - number of stop words stored in words[]
// output: line - with all stop words removed
// return: an integer for number of words removed from line
//
int RemoveAllStopwordsFromLine(string &line, Stop_word words[], int num_words) {
    int count = 0;
    for (int i = 0; i < num_words; i++) {
        int wordCount = RemoveWordFromLine(line, words[i].word);
        words[i].count += wordCount;
        count += wordCount;
    }
    return count;
}


int main() {

    Stop_word stopwords[MAX_NUM_STOPWORDS];     // an array of struct Stop_word

    // read in two filenames from user input
    string stop_words_filename, story_filename;
    cin >> stop_words_filename >> story_filename;

    // read stop words from stopword file and
    // store them in an array of struct Stop_word
    int num_words;
    ReadStopWordFromFile(stop_words_filename, stopwords, num_words);

    // open text file
    ifstream story_file(story_filename);
    if (!story_file.is_open()) {
        cout << "Failed to open " << story_filename << endl;
        exit(0);
    }

    // open cleaned text file
    ofstream story_cleaned_file(story_filename.substr(0, story_filename.length() - 4) + "_cleaned.txt");
    if (!story_cleaned_file.is_open()) {
        cout << "Failed to open " << story_filename.substr(0, story_filename.length() - 4) + "_cleaned.txt" << endl;
        exit(0);
    }

    // read in each line from text file, remove stop words,
    // and write to output cleaned text file
    string line;
    int total_remove_count = 0;
    bool first_line = true;
    while (getline(story_file, line)) {

        total_remove_count += RemoveAllStopwordsFromLine(line, stopwords, num_words);
        story_cleaned_file << line << endl;
    }


    // close text file and cleaned text file
    story_file.close();
    story_cleaned_file.close();

    // write removal count of stop words to files
    WriteStopWordCountToFile(stop_words_filename.substr(0, stop_words_filename.length() - 4) + "_count.txt", stopwords,
                             num_words);

    // output to screen total number of words removed
    cout << "Number of stop words removed = " << total_remove_count << endl;

    return 0;
}
