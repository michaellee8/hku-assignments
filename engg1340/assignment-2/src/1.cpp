//
//  Assignment 2 Task 1 template
//  Copyright © 2019 HKU ENGG1340. All rights reserved.
//

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

const double e = 2.72;
const int MAX_T = 100;

///////// DO NOT MODIFY ANYTHING ABOVE THIS LINE /////////

// IMPORTANT:  Do NOT change any of the function headers
//             It means that you will need to use the function headers as is

// You may implement additional functions here


// Function: sigmoid activation function
// Input: double x: the input of sigmoid activation function
// Ouput: the output of sigmoid activation function
double sigmoid(double x) {
    return 1 / (1 + exp(-x * log(e)));
}

// Function: tanh activate function
// Input: double x: the input of tanh activation function
// Ouput: double: the output of tanh activation function.
double tanh(double x) {
    return 2 * sigmoid(2 * x) - 1;
}

// Function: compute the next hidden value in an RNN cell
// Input: double x: current input value
//        double h: current hidden status in RNN cell
// Ouput: double: next hidden value in RNN cell
double ComputeH(double x, double h) {
    return tanh(0.5 * x - 2 * h);
}

// Function: compute the output value at current time step
// Input: double x: current input value
//        double h: current hidden status in RNN cell
// Ouput: double: current output value
double ComputeO(double x, double h) {
    return sigmoid(0.1 * x + 1.5 * h);

}

// Function: print the values stored in a 1D-array to screen
// Input: double xs[]: the value of the sequence
//        int seq_len: the length of the sequence
void PrintSeqs(double xs[], int seq_len) {
    for (int i = 0; i < seq_len; i++) {
        if (i != 0) {
            cout << " ";
        }
        cout << fixed << setprecision(10) << xs[i];
    }
    cout << endl;
}

// Function: main function
int main() {
    int T;
    float h_0;
    cin >> T >> h_0;
    double x[MAX_T];
    for (int i = 0; i < T; i++) {
        cin >> x[i];
    }
    double h[MAX_T];
    double o[MAX_T];
    for (int i = 0; i < T; i++) {
        h[i] = ComputeH(x[i], i == 0 ? h_0 : h[i - 1]);
        o[i] = ComputeO(x[i], i == 0 ? h_0 : h[i - 1]);
    }
    PrintSeqs(h, T);
    PrintSeqs(o, T);
}
