//
//  Assignment 2 Task 4 template
//  Copyright © 2019 HKU ENGG1340. All rights reserved.
//

#include <stdio.h>

const int MAX_INT_LENGTH = 100;

///////// DO NOT MODIFY ANYTHING ABOVE THIS LINE /////////

// IMPORTANT:  Do NOT change any of the function headers already provided to you
//             It means that you will need to use the function headers as is


// You may implement additional functions here



int charToDigit(char c) {
    return (int) c - 48;
}

char digitToChar(int i) {
    return (char) (i + 48);
}

int str_len(char *s) {
    for (int i = 0; 1; i++) {
        if (s[i] == '\0') {
            return i;
        }
    }
}

void str_cpy(char in[], char out[], int offset) {
    int i = 0;
    while (1) {
        out[i] = in[i + offset];
        if (in[i] == '\0') {
            break;
        }
        i++;
    }
}

//
// AddTwoBigNumbers: to sum up two big numbers represented as digits in a char clear_char_array
//
// input:  char bigN[], char bigM[]:  two large numbers
// output the sum as a big number in the input array char sum[]
void AddTwoBigNumbers(char bigN[], char bigM[], char sum[]) {
    for (int i = 0; i < MAX_INT_LENGTH + 1; i++) {
        sum[i] = '0';
    }
    int nlen = str_len(bigN);
    int mlen = str_len(bigM);
    int minLen = nlen < mlen ? nlen : mlen;
    int maxLen = nlen > mlen ? nlen : mlen;
    sum[maxLen + 1] = '\0';
    int nextDigit = 0;
    for (int i = 0; i < minLen; i++) {
        int digit = nextDigit + charToDigit(bigN[nlen - 1 - i]) + charToDigit(bigM[mlen - 1 - i]);
        sum[maxLen - i] = digitToChar(digit % 10);
        nextDigit = digit / 10;
    }
    for (int i = minLen; i < maxLen; i++) {
        int digit;
        if (nlen == maxLen) {
            // n is larger
            digit = nextDigit + charToDigit(bigN[nlen - 1 - i]);
        } else {
            digit = nextDigit + charToDigit(bigM[mlen - 1 - i]);
        }
        sum[maxLen - i] = digitToChar(digit % 10);
        nextDigit = digit / 10;
    }
    sum[0] = digitToChar(nextDigit);
    if (sum[0] == '0') {
        char tmp[MAX_INT_LENGTH + 1];
        str_cpy(sum, tmp, 0);
        str_cpy(tmp, sum, 1);
    }

}


///////// DO NOT MODIFY ANYTHING BELOW THIS LINE /////////

// Function: main function
// ==============================================================
int main() {
    char bignum[2][MAX_INT_LENGTH]; // bignum[0] and bignum[1] are to store the digits of the two input number
    char sum[MAX_INT_LENGTH + 1];     // to store the sum of the two big numbers

    // read in two numbers
    scanf("%s", bignum[0]);
    scanf("%s", bignum[1]);

    // calculate sum of the two numbers
    AddTwoBigNumbers(bignum[0], bignum[1], sum);

    // display the sum on screen
    printf("%s\n", sum);


    return 0;
}
