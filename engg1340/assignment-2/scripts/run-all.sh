#!/usr/bin/env bash

# This script builds all the solution and run all of the test cases.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

${DIR}/run-q.sh 1 3
${DIR}/run-q.sh 2 6
${DIR}/run-q-fs.sh 3 2
${DIR}/run-q-c.sh 4 3
exit 0