#!/usr/bin/env bash

# This scripts runs all of the test case of a specific question
# $1: question number
# $2: number of test cases

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
${DIR}/build.sh $1
for i in $(eval echo "{1..$2}"); do
  ${DIR}/run-case-fs.sh $1 "${i}"
done
