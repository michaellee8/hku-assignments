#!/usr/bin/env bash

# This script builds a specific file into an executable

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [[ -e ${DIR}/../out/$1.run ]];then
  rm ${DIR}/../out/$1.run
fi
gcc ${DIR}/../src/$1.c -o ${DIR}/../out/$1.run
